﻿using System;
using FishTank.Model.Fishes;

namespace FishTank.Model
{
    /// <summary>
    /// Creates a Fish.
    /// </summary>
    public static class FishFactory
    {
        #region Enum

        private enum FishType
        {
            Shark,
            StingFish,
            SpottedStingFish,
            TigerShark,
            CheepCheep
        }

        #endregion

        #region Methods

        /// <summary>
        /// Creates the fish.
        /// </summary>
        /// <returns>
        /// a Fish
        /// </returns>
        public static Fish CreateFish()
        {
            Fish theFish;

            var randomFishSprite = determineRandomFish();

            switch (randomFishSprite)
            {
                case FishType.Shark:
                    theFish = new Shark();
                    break;
                case FishType.TigerShark:
                    theFish = new TigerShark();
                    break;
                case FishType.StingFish:
                    theFish = new StingFish();
                    break;
                case FishType.SpottedStingFish:
                    theFish = new SpottedStingFish();
                    break;
                case FishType.CheepCheep:
                    theFish = new CheepCheep();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return theFish;
        }

        private static FishType determineRandomFish()
        {
            var values = Enum.GetValues(typeof (FishType));
            var randomShape = (FishType) RandomGenerator.GetNextInt(0, values.Length);
            return randomShape;
        }

        #endregion
    }
}
