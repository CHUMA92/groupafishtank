﻿using System.Drawing;

namespace FishTank.Model.SwimStrategies
{
    /// <summary>
    ///     Holds information about the resting swim strategy
    /// </summary>
    public class RestingSwimStrategy : ISwimStrategy
    {
        private int distanceMovedHorizontal;
        private int distanceMovedVertical;

        /// <summary>
        /// Initializes a new instance of the <see cref="RestingSwimStrategy"/> class.
        /// </summary>
        public RestingSwimStrategy()
        {
            this.distanceMovedHorizontal = 0;
            this.distanceMovedVertical = 0;
        }

        /// <summary>
        /// Swims this instance.
        /// </summary>
        /// <returns></returns>
        public Point Swim()
        {
            var pixelsToMoveX = this.getRandomSmallAmountOfPixelsToMove();
            var pixelsToMoveY = this.getRandomSmallAmountOfPixelsToMove();


            pixelsToMoveX = this.determineHorizontalDistanceToSwim(pixelsToMoveX);

            pixelsToMoveY = this.determineVerticalDistanceToSwim(pixelsToMoveY);

            var point = new Point(pixelsToMoveX, pixelsToMoveY);

            return point;
        }

        private int determineVerticalDistanceToSwim(int pixelsToMoveY)
        {
            if (this.distanceMovedVertical + pixelsToMoveY > 4 || this.distanceMovedVertical + pixelsToMoveY < -4)
            {
                pixelsToMoveY = 0;
            }
            else
            {
                this.distanceMovedVertical += pixelsToMoveY;
            }
            return pixelsToMoveY;
        }

        private int determineHorizontalDistanceToSwim(int pixelsToMoveX)
        {
            if (this.distanceMovedHorizontal + pixelsToMoveX > 4 || this.distanceMovedHorizontal + pixelsToMoveX < -4)
            {
                pixelsToMoveX = 0;
            }
            else
            {
                this.distanceMovedHorizontal += pixelsToMoveX;
            }
            return pixelsToMoveX;
        }

        private int getRandomSmallAmountOfPixelsToMove()
        {
            const int minimumAmountToMove = -1;
            const int maximumAmountToMove = 1;

            var spacesToMove = RandomGenerator.GetNextInt(minimumAmountToMove, maximumAmountToMove + 1);

            return spacesToMove;
        }

    }
}
