﻿using System.Drawing;

namespace FishTank.Model.SwimStrategies
{
    /// <summary>
    ///     Holds information about the ISwim strategy interface.
    /// </summary>
    public interface ISwimStrategy
    {
        /// <summary>
        /// Swims this instance.
        /// </summary>
        /// <returns></returns>
        Point Swim();
    }
}
