﻿using System.Drawing;

namespace FishTank.Model.SwimStrategies
{
    /// <summary>
    ///     Holds information about the swim to bottom strategy
    /// </summary>
    public class SwimToBottomStrategy : ISwimStrategy
    {
        /// <summary>
        /// Swims this instance.
        /// </summary>
        /// <returns></returns>
        public Point Swim()
        {
            var moveHorizontal = this.determineMovemoveHorizontal();

            const int minimumAmountOfPixelsToMove = 4;
            const int maximumAmountOfPixelsToMove = 6;

            var pixelsToMoveY = this.getRandomAmountOfPixelsToMoveFish(minimumAmountOfPixelsToMove, maximumAmountOfPixelsToMove);
            var pixelsToMoveX = this.determineHorizontalMovement(moveHorizontal);

            var point = new Point(pixelsToMoveX, pixelsToMoveY);

            return point;
        }

        private int determineHorizontalMovement(bool moveHorizontal)
        {
            var pixelsToMoveX = 0;

            if (!moveHorizontal)
            {
                return pixelsToMoveX;
            }
            const int minimumAmountOfPixelsToMove = -7;
            const int maximumAmountOfPixelsToMove = 7;
            pixelsToMoveX = this.getRandomAmountOfPixelsToMoveFish(minimumAmountOfPixelsToMove, maximumAmountOfPixelsToMove);
            return pixelsToMoveX;
        }

        private bool determineMovemoveHorizontal()
        {
            var chosenDecision = false;
            const int percentageOfChoices = 10;
            const int percentToMoveVertical = 7;

            var randomPercentage = RandomGenerator.GetNextInt(percentageOfChoices) + 1;

            if (percentToMoveVertical >= randomPercentage)
            {
                chosenDecision = true;
            }

            return chosenDecision;
        }

        private int getRandomAmountOfPixelsToMoveFish(int minimumAmountToMove, int maximumAmountToMove)
        {
            var spacesToMove = RandomGenerator.GetNextInt(minimumAmountToMove, maximumAmountToMove + 1);

            return spacesToMove;
        }
    }
}
