﻿using System.Drawing;
using FishTank.Model.Fishes;

namespace FishTank.Model.SwimStrategies
{
    /// <summary>
    ///     Holds information about the random swim strategy
    /// </summary>
    public class SwimRandomAmountVertically : ISwimStrategy
    {

        private int randomAmountToSwimUp;
        private readonly Fish movingFish;

        /// <summary>
        /// Initializes a new instance of the <see cref="SwimRandomAmountVertically"/> class.
        /// </summary>
        /// <param name="canvasHeight">Height of the canvas.</param>
        /// <param name="fish">The fish.</param>
        public SwimRandomAmountVertically(int canvasHeight, Fish fish)
        {
            this.movingFish = fish;

            var maximumAmountToMove = canvasHeight - this.movingFish.Height;
            const int minimumAmountToMove = 10;
            this.randomAmountToSwimUp = RandomGenerator.GetNextInt(minimumAmountToMove, maximumAmountToMove);
        }

        /// <summary>
        /// Swims this instance.
        /// </summary>
        /// <returns></returns>
        public Point Swim()
        {
            var moveHorizontal = this.determineMovemoveHorizontal();

            var pixelsToMoveY = this.handleMovementY();

            var pixelsToMoveX = this.handleMovementX(moveHorizontal, pixelsToMoveY);

            pixelsToMoveY = pixelsToMoveY * -1;

            var point = new Point(pixelsToMoveX, pixelsToMoveY);

            return point;
        }

        private int handleMovementX(bool moveHorizontal, int pixelsToMoveY)
        {
            var pixelsToMoveX = 0;

            pixelsToMoveX = this.handlesFishHorizontalMovement(moveHorizontal, pixelsToMoveX);

            this.randomAmountToSwimUp -= pixelsToMoveY;
            return pixelsToMoveX;
        }

        private int handleMovementY()
        {
            const int minimumAmountOfPixelsToMove = 5;
            const int maximumAmountOfPixelsToMove = 10;

            var pixelsToMoveY = this.getRandomAmountOfPixelsToMoveFish(minimumAmountOfPixelsToMove,
                maximumAmountOfPixelsToMove);

            pixelsToMoveY = this.checkFishAtMaximumHeight(pixelsToMoveY);
            return pixelsToMoveY;
        }

        private int handlesFishHorizontalMovement(bool moveHorizontal, int pixelsToMoveX)
        {
            if (!moveHorizontal)
            {
                return pixelsToMoveX;
            }
            const int minimumAmountOfPixelsToMove = -7;
            const int maximumAmountOfPixelsToMove = 7;
            pixelsToMoveX = this.getRandomAmountOfPixelsToMoveFish(minimumAmountOfPixelsToMove,
                maximumAmountOfPixelsToMove);
            return pixelsToMoveX;
        }

        private int checkFishAtMaximumHeight(int pixelsToMoveY)
        {
            if (this.randomAmountToSwimUp - pixelsToMoveY >= 0)
            {
                return pixelsToMoveY;
            }
            pixelsToMoveY = this.randomAmountToSwimUp;
            this.movingFish.StrategyMoveRandomVerticalDone();
            return pixelsToMoveY;
        }

        private bool determineMovemoveHorizontal()
        {
            var chosenDecision = false;
            const int percentageOfChoices = 10;
            const int percentToMoveVertical = 7;

            var randomPercentage = RandomGenerator.GetNextInt(percentageOfChoices) + 1;

            if (percentToMoveVertical >= randomPercentage)
            {
                chosenDecision = true;
            }

            return chosenDecision;
        }

        private int getRandomAmountOfPixelsToMoveFish(int minimumAmountToMove, int maximumAmountToMove)
        {
            var spacesToMove = RandomGenerator.GetNextInt(minimumAmountToMove, maximumAmountToMove + 1);

            return spacesToMove;
        }
    }
}
