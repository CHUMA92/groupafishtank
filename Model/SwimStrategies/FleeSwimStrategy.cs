﻿using System.Drawing;

namespace FishTank.Model.SwimStrategies
{
    /// <summary>
    ///     Holds information about the flee swim strategy
    /// </summary>
    internal class FleeSwimStrategy : ISwimStrategy
    {
        /// <summary>
        /// Swims this instance.
        /// </summary>
        /// <returns></returns>
        public Point Swim()
        {
            var moveVertical = this.determineMoveVertical();

            const int minimumAmountOfPixelsToMove = 12;
            const int maximumAmountOfPixelsToMove = 15;

            var pixelsToMoveX = this.getRandomAmountOfPixelsToMoveFish(minimumAmountOfPixelsToMove,
                maximumAmountOfPixelsToMove);
            var pixelsToMoveY = 0;

            pixelsToMoveY = this.determinesTheDistanceToMoveVertically(moveVertical, pixelsToMoveY);

            var point = new Point(pixelsToMoveX, pixelsToMoveY);

            return point;
        }

        private int determinesTheDistanceToMoveVertically(bool moveVertical, int pixelsToMoveY)
        {
            if (!moveVertical)
            {
                return pixelsToMoveY;
            }
            const int minimumAmountOfPixelsToMove = -6;
            const int maximumAmountOfPixelsToMove = 6;
            pixelsToMoveY = this.getRandomAmountOfPixelsToMoveFish(minimumAmountOfPixelsToMove,
                maximumAmountOfPixelsToMove);
            return pixelsToMoveY;
        }

        private bool determineMoveVertical()
        {
            var chosenDecision = false;
            const int percentageOfChoices = 10;
            const int percentToMoveVertical = 8;

            var randomPercentage = RandomGenerator.GetNextInt(percentageOfChoices) + 1;

            if (percentToMoveVertical >= randomPercentage)
            {
                chosenDecision = true;
            }
            return chosenDecision;
        }

        private int getRandomAmountOfPixelsToMoveFish(int minimumAmountToMove, int maximumAmountToMove)
        {
            var spacesToMove = RandomGenerator.GetNextInt(minimumAmountToMove, maximumAmountToMove + 1);

            return spacesToMove;
        }
    }
}
