﻿using System.Drawing;
using FishTank.View.Sprites;

namespace FishTank.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class FishFood
    {
        private const int FoodWidth = 6;
        private const int FoodHeight = 6;
        private int x;
        private readonly FishFoodSprite foodSprite;
        private Rectangle collisionRectangle;

        /// <summary>
        /// Gets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public int Y { get; private set; }

        #region Methods

        /// <summary>
        /// Gets the collision box.
        /// </summary>
        /// <value>
        /// The collision box.
        /// </value>
        public Rectangle CollisionBox
        {
            get { return this.collisionRectangle; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FishFood"/> class.
        /// </summary>
        /// <param name="initialX">The initial x.</param>
        /// <param name="initialY">The initial y.</param>
        public FishFood(int initialX, int initialY)
        {
            this.x = initialX;
            this.Y = initialY;
            this.collisionRectangle = new Rectangle(this.x, this.Y, FoodWidth, FoodHeight);
            this.foodSprite = new FishFoodSprite(FoodWidth, FoodHeight);
        }

        /// <summary>
        /// Updates the specified graphics.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="canvasWidth">Width of the canvas.</param>
        /// <param name="canvasHeight">Height of the canvas.</param>
        public void Update(Graphics graphics, int canvasWidth, int canvasHeight)
        {
            this.move(canvasWidth);
            this.paint(graphics);
        } 

        #endregion

        #region Helper Methods

        private void move(int canvasWidth)
        {
            this.determineXMovement(canvasWidth);

            this.Y += RandomGenerator.GetNextInt(1, 3);

            this.updateCollisionBox();
        }

        private void determineXMovement(int canvasWidth)
        {
            this.x += RandomGenerator.GetNextInt(-3, 3);

            if (this.x < 0)
            {
                this.x = 0;
            }

            if (this.x + FoodWidth > canvasWidth)
            {
                this.x = canvasWidth - FoodWidth;
            }
        }

        private void updateCollisionBox()
        {
            this.collisionRectangle.X = this.x;
            this.collisionRectangle.Y = this.Y;
        }

        private void paint(Graphics graphics)
        {
            this.foodSprite.Paint(graphics, this.x, this.Y);
        } 

        #endregion
    }
}
