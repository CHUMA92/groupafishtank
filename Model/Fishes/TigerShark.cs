﻿using System.Drawing;
using FishTank.View.Sprites.FishSprites;

namespace FishTank.Model.Fishes
{
    /// <summary>
    ///     Holds information about a tiger shark.
    /// </summary>
    public class TigerShark : Shark
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TigerShark"/> class.
        /// </summary>
        public TigerShark()
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TigerShark"/> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        public TigerShark(Point location)
            : base(location)
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TigerShark"/> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public TigerShark(int x, int y)
            : base(x, y)
        {
            this.initializeMembers();
        }

        private void initializeMembers()
        {
            Sprite = new TigerSharkSprite(this);
            SetRandomWeightAndScaleSpriteToWeight();
        }
    }
}
