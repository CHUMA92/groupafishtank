﻿using System.Drawing;
using FishTank.View.Sprites.FishSprites;

namespace FishTank.Model.Fishes
{
    /// <summary>
    ///     Holds information about a spotted fish.
    /// </summary>
    public class SpottedStingFish : StingFish
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpottedStingFish"/> class.
        /// </summary>
        public SpottedStingFish()
        {
            Sprite = new SpottedStingFishSprite(this);
            SetRandomWeightAndScaleSpriteToWeight();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpottedStingFish"/> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        public SpottedStingFish(Point location)
            : base(location)
        {
            Sprite = new SpottedStingFishSprite(this);
            SetRandomWeightAndScaleSpriteToWeight();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpottedStingFish"/> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public SpottedStingFish(int x, int y)
            : base(x, y)
        {
            Sprite = new SpottedStingFishSprite(this);
            SetRandomWeightAndScaleSpriteToWeight();

        }
    }
}
