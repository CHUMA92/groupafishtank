using System;
using System.Drawing;
using FishTank.Model.SwimStrategies;
using FishTank.View.Sprites;

namespace FishTank.Model.Fishes
{
    /// <summary>
    /// Holds information about a fish.
    /// </summary>
    public abstract class Fish
    {
        #region Data members

        private Point location;
        private const int MinimumMovement = 3;
        private const int MaximumMovement = 8;
        private ISwimStrategy strategy;
        private int biteCoolDown;
        private int currentTankLap;
        private Rectangle collisionBox;

        #endregion

        #region Enum

        /// <summary>
        /// Direction of the fish
        /// </summary>
        public enum Direction
        {
            /// <summary>
            /// The left
            /// </summary>
            Left,
            /// <summary>
            /// The right
            /// </summary>
            Right
        }

        /// <summary>
        /// 
        /// </summary>
        public enum FishStatus
        {
            /// <summary>
            /// The scared
            /// </summary>
            Tired,
            /// <summary>
            /// The dead
            /// </summary>
            Dead,
            /// <summary>
            /// The resting
            /// </summary>
            Resting,
            /// <summary>
            /// The neutral
            /// </summary>
            Neutral
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the bite count.
        /// </summary>
        /// <value>
        /// The bite count.
        /// </value>
        public int BiteCount { get; private set; }

        /// <summary>
        /// Gets the facing direction.
        /// </summary>
        /// <value>
        /// The facing direction.
        /// </value>
        public Direction FacingDirection { get; protected set; }

        /// <summary>
        /// Gets or sets the move foward.
        /// </summary>
        /// <value>
        /// The move foward.
        /// </value>
        public double MoveFoward { get; protected set; }

        /// <summary>
        /// Gets or sets the move backward.
        /// </summary>
        /// <value>
        /// The move backward.
        /// </value>
        public double MoveBackward { get; protected set; }

        /// <summary>
        /// Gets or sets the move upward.
        /// </summary>
        /// <value>
        /// The move upward.
        /// </value>
        public double MoveUpward { get; protected set; }

        /// <summary>
        /// Gets or sets the move downward.
        /// </summary>
        /// <value>
        /// The move downward.
        /// </value>
        public double MoveDownward { get; protected set; }

        /// <summary>
        /// Gets the random movement.
        /// </summary>
        /// <value>
        /// The random movement.
        /// </value>
        protected int RandomMovement { get; private set; }

        /// <summary>
        ///     Gets or sets the x location of the fish.
        /// </summary>
        /// <value>
        ///     The x.
        /// </value>
        public int X
        {
            get { return this.location.X; }
            set { this.location.X = value; }
        }

        /// <summary>
        ///     Gets or sets the y location of the fish.
        /// </summary>
        /// <value>
        ///     The y.
        /// </value>
        public int Y
        {
            get { return this.location.Y; }
            set { this.location.Y = value; }
        }

        /// <summary>
        /// Gets the sprite.
        /// </summary>
        /// <value>
        /// The sprite.
        /// </value>
        protected FishSprite Sprite { private get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height
        {
            get { return this.Sprite.AdjustedHeight; }
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width
        {
            get { return this.Sprite.AdjustedWidth; }
        }

        /// <summary>
        /// Gets or sets the maximum weight.
        /// </summary>
        /// <value>
        /// The maximum weight.
        /// </value>
        protected double MaxWeight { get; set; }

        /// <summary>
        /// Gets or sets the minimum weight.
        /// </summary>
        /// <value>
        /// The minimum weight.
        /// </value>
        protected double MinWeight { get; set; }

        /// <summary>
        /// Gets or sets the collsion box.
        /// </summary>
        /// <value>
        /// The collsion box.
        /// </value>
        public Rectangle CollsionBox
        {
            get { return this.collisionBox; }
            protected set { this.collisionBox = value; }
        }

        /// <summary>
        /// Gets and Sets the Fish's weight.
        /// </summary>
        /// <value>
        /// The weight.
        /// </value>
        public double Weight { get; private set; }

        /// <summary>
        /// Gets and Sets the Fish's weight percent.
        /// </summary>
        /// <value>
        /// The weight percent.
        /// </value>
        private double WeightPercentage { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public FishStatus Status { get; private set; }

        /// <summary>
        /// Gets or sets the default strategy.
        /// </summary>
        /// <value>
        /// The default strategy.
        /// </value>
        protected ISwimStrategy DefaultStrategy { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="Fish" /> class from being created.
        /// </summary>
        protected Fish()
        {
            this.Sprite = null;
            this.initializeMembers();
        }

        /// <summary>
        ///     Constructs a fish at specified location
        ///     Precondition: location != null
        ///     Postcondition: X == location.X; Y == location.Y
        /// </summary>
        /// <param name="location">Location to create fish</param>
        protected Fish(Point location)
            : this()
        {

            if (location == null)
            {
                throw new ArgumentNullException("location");
            }

            this.location = location;
            this.initializeMembers();
        }

        /// <summary>
        ///     Constructs a fish at specified x,y location
        ///     Precondition: None
        ///     Postcondition: X == x; Y == y
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        protected Fish(int x, int y)
            : this()
        {
            this.location = new Point(x, y);
            this.initializeMembers();
        }

        private void initializeMembers()
        {
            this.determineFacingUponInitialization();
            this.RandomMovement = RandomGenerator.GetNextInt(MinimumMovement, MaximumMovement + 1);
            const int xInitial = 0;
            const int yInitial = 0;
            const int widthInitial = 1;
            const int heightInitial = 1;
            this.CollsionBox = new Rectangle(xInitial, yInitial, widthInitial, heightInitial);
            this.Status = FishStatus.Neutral;
           
            if (GetType() == typeof(Shark) || GetType() == typeof(TigerShark))
            {
                this.currentTankLap = 1;
                this.biteCoolDown = 0;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the aStrategy.
        /// </summary>
        /// <param name="aStrategy">The aStrategy.</param>
        public void SetStrategy(ISwimStrategy aStrategy)
        {
            this.strategy = aStrategy;
        }

        /// <summary>
        /// Updates the specified g.
        /// </summary>
        /// <param name="g">The g.</param>
        /// <param name="canvasWidth">Width of the canvas.</param>
        /// <param name="canvasHeight">Height of the canvas.</param>
        public void Update(Graphics g, int canvasWidth, int canvasHeight)
        {
            this.Move(canvasWidth, canvasHeight);
            this.Paint(g);

            if (this.biteCoolDown > 0)
            {
                this.biteCoolDown -= 1;
            }
        }

        /// <summary>
        /// Sets to default strategy.
        /// </summary>
        public void SetToDefaultStrategy()
        {
            this.Status = FishStatus.Neutral;
            this.SetStrategy(this.DefaultStrategy);
        }

        /// <summary>
        /// Randomly moves a fish in the x, y direction
        /// Precondition: None
        /// Postcondition: X == X@prev + amount of movement in X direction; Y == Y@prev + amount of movement in Y direction
        /// </summary>
        public void Move(int canvasWidth, int canvasHeight)
        {
            var movePoints = this.strategy.Swim();

            this.handleMovingHorizontal(movePoints.X);
            this.handleMovingVertical(movePoints.Y);

            this.keepFishInBoundsIfBeyondCanvas(canvasWidth, canvasHeight);
            this.updateCollisionBox();
        }


        /// <summary>
        /// Sets the fish positions.
        /// </summary>
        /// <param name="xPosition">The x position.</param>
        /// <param name="yPosition">The y position.</param>
        public void SetFishPositions(int xPosition, int yPosition)
        {
            this.X = xPosition;
            this.Y = yPosition;
            this.updateCollisionBox();
        }

        /// <summary>
        /// Handles the fish bite.
        /// </summary>
        public void HandleWhenSharkBitesFish()
        {
            const int maxBiteCoolDown = 50;

            if (GetType() != typeof(Shark) && GetType() != typeof(TigerShark))
            {
                this.changeStrategyWhenBiten();
            }
            else
            {
                this.handleIncreaseInSizeOfFish();
                this.biteCoolDown = maxBiteCoolDown;
            }
        }

        /// <summary>
        /// Handles the fish eat.
        /// </summary>
        public void HandleWhenFishEatFood()
        {
            this.handleIncreaseInSizeOfFish();
        }

        /// <summary>
        /// Wakes the shark from resting.
        /// </summary>
        /// <param name="canvasHeight">Height of the canvas.</param>
        public void WakeSharkFromResting(int canvasHeight)
        {
            this.SetStrategy(new SwimRandomAmountVertically(canvasHeight, this));
        }

        /// <summary>
        /// Checks the shark can bite.
        /// </summary>
        /// <returns></returns>
        public bool CheckSharkCanBite()
        {
            var canBite = this.biteCoolDown == 0 && this.Status != FishStatus.Resting && this.Status != FishStatus.Tired;

            return canBite;
        }

        /// <summary>
        /// Draws a fish
        /// Precondition: g != NULL
        /// </summary>
        /// <param name="g">The graphics object to draw the fish on</param>
        /// <exception cref="ArgumentNullException">g</exception>
        public void Paint(Graphics g)
        {
            if (g == null)
            {
                throw new ArgumentNullException("g");
            }
            this.Sprite.Paint(g);
        }

        /// <summary>
        /// Strategies the move random vertical done.
        /// </summary>
        public void StrategyMoveRandomVerticalDone()
        {
            if (GetType() == typeof(Shark) || GetType() == typeof(TigerShark))
            {
                this.Status = FishStatus.Neutral;
                this.SetStrategy(new AggressiveSwimStrategy());
            }
            else
            {
                this.SetStrategy(new FleeSwimStrategy());
            }

        }

        /// <summary>
        /// Changes the direction.
        /// </summary>
        public void ChangeDirection()
        {
            if (this.FacingDirection == Direction.Right)
            {
                this.FacingDirection = Direction.Left;
            }
            else
            {
                this.FacingDirection = Direction.Right;
            }
        }

        /// <summary>
        /// Sets the Fish's weight to a random value between its max and min weights
        /// and scales its FishSprite depending on the random value retrieved.
        /// </summary>
        protected void SetRandomWeightAndScaleSpriteToWeight()
        {
            this.WeightPercentage = RandomGenerator.GetNextDouble();

            this.Weight = (this.MinWeight + ((this.MaxWeight - this.MinWeight) * this.WeightPercentage));

            this.Sprite.SetSpriteScaledSizeMeasurements(this.WeightPercentage);

            this.updateCollisionBox();
        }

        #endregion

        #region Helper Methods

        private void updateCollisionBox()
        {
            this.collisionBox.X = this.X;
            this.collisionBox.Y = this.Y;
            this.collisionBox.Width = this.Width;
            this.collisionBox.Height = this.Height;
        }

        private void keepFishInBoundsIfBeyondCanvas(int canvasWidth, int canvasHeight)
        {
            this.handleXBoundary(canvasWidth);
            this.handleYBoundary(canvasHeight);
        }

        private void handleXBoundary(int canvasWidth)
        {
            if (this.X < 0)
            {
                this.handlesCollisionDetectionForLeftBarrier();
            }
            else if (this.X + this.Width > canvasWidth)
            {
                this.handlesCollisionDetectionForRightBarrier(canvasWidth);
            }
        }

        private void handlesCollisionDetectionForRightBarrier(int canvasWidth)
        {
            this.X = canvasWidth - this.Width;

            if (this.FacingDirection == Direction.Right)
            {
                this.ChangeDirection();
            }

            if (GetType() == typeof(Shark) || GetType() == typeof(TigerShark))
            {
                this.handleWhenSharkHitsBoundary();
            }
        }

        private void handlesCollisionDetectionForLeftBarrier()
        {
            this.X = 0;

            if (this.FacingDirection == Direction.Left)
            {
                this.ChangeDirection();
            }

            if (GetType() == typeof(Shark) || GetType() == typeof(TigerShark))
            {
                this.handleWhenSharkHitsBoundary();
            }
        }

        private void handleYBoundary(int canvasHeight)
        {
            if (this.Y < 0)
            {
                this.handlesCollisionDetectionForBottomBarrier();
            }
            else if (this.Y + this.Height > canvasHeight)
            {
                this.handlesCollisionDetectionForBottomBarrier(canvasHeight);
            }
        }

        private void handlesCollisionDetectionForBottomBarrier(int canvasHeight)
        {
            this.Y = canvasHeight - this.Height;
            if (this.Status == FishStatus.Tired)
            {
                this.Status = FishStatus.Resting;
                this.SetStrategy(new RestingSwimStrategy());
            }
        }

        private void handlesCollisionDetectionForBottomBarrier()
        {
            this.Y = 0;

            if (this.Status == FishStatus.Dead)
            {
                this.SetStrategy(new RestingSwimStrategy());
            }
        }

        private void handleMovingHorizontal(int pixelsToMoveX)
        {
            if (this.FacingDirection == Direction.Right)
            {
                this.X += pixelsToMoveX;
            }
            else
            {
                this.X -= pixelsToMoveX;
            }
        }

        private void handleMovingVertical(int pixelsToMoveY)
        {
            this.Y += pixelsToMoveY;
        }

        private void changeStrategyWhenBiten()
        {
            this.BiteCount += 1;

            const int numOfBitesTillScared = 2;

            this.handleBiteCountStrategies(numOfBitesTillScared);
        }

        private void handleBiteCountStrategies(int numOfBitesTillScared)
        {
            if (this.BiteCount == numOfBitesTillScared)
            {
                this.SetStrategy(new FleeSwimStrategy());
            }
            else if (this.BiteCount > numOfBitesTillScared)
            {
                this.Status = FishStatus.Dead;
                this.SetStrategy(new SwimToTopStrategy());
            }
        }

        private void determineFacingUponInitialization()
        {
            const int numberOfDirections = 2;

            const int initialDirection = 0;
            var choosenDirection = RandomGenerator.GetNextInt(numberOfDirections);

            this.chooseInitialDirection(choosenDirection, initialDirection);
        }

        private void chooseInitialDirection(int choosenDirection, int initialDirection)
        {
            if (choosenDirection == initialDirection)
            {
                this.FacingDirection = Direction.Right;
            }
            else
            {
                this.FacingDirection = Direction.Left;
            }
        }

        private void handleWhenSharkHitsBoundary()
        {
            if (this.Status != FishStatus.Resting && this.Status != FishStatus.Tired)
            {
                if (this.currentTankLap != 3)
                {
                    this.currentTankLap += 1;
                }
                else
                {
                    this.currentTankLap = 1;

                    this.Status = FishStatus.Tired;
                    this.SetStrategy(new SwimToBottomStrategy());
                }
            }
        }

        /// <summary>
        /// Increases the weight of this fish based on the type of Fish this object is.
        /// </summary>
        private void handleIncreaseInSizeOfFish()
        {
            this.increaseFishWeight();

            this.Sprite.SetSpriteScaledSizeMeasurements(this.WeightPercentage);
            this.updateCollisionBox();
        }

        private void increaseFishWeight()
        {
            var increaseInWeight = this.getIncreaseInWeightBasedOnFishType();

            if (this.Weight + ((this.MaxWeight - this.MinWeight) * increaseInWeight) < this.MaxWeight)
            {
                this.Weight += ((this.MaxWeight - this.MinWeight) * increaseInWeight);
                this.WeightPercentage += increaseInWeight;
            }
            else
            {
                this.Weight = this.MaxWeight;
                this.WeightPercentage = 1;
            }
        }

        private double getIncreaseInWeightBasedOnFishType()
        {
            double increaseInWeight;

            if (GetType() == typeof(Shark) || GetType() == typeof(TigerShark))
            {
                increaseInWeight = .02;
            }
            else
            {
                increaseInWeight = .03;
            }

            return increaseInWeight;
        }

        #endregion
    }
}