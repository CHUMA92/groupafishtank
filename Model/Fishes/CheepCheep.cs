﻿using System.Drawing;
using FishTank.Model.SwimStrategies;
using FishTank.View.Sprites.FishSprites;

namespace FishTank.Model.Fishes
{
    /// <summary>
    /// Holds information about a cheep-cheep fish.
    /// </summary>
    public class CheepCheep : Fish
    {
        private const double CheepMinWeight = 1.5;
        private const double CheepMaxWeight = 20.5;

        /// <summary>
        /// Initializes a new instance of the <see cref="Shark"/> class.
        /// </summary>
        public CheepCheep()
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shark"/> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        public CheepCheep(Point location)
            : base(location)
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shark"/> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public CheepCheep(int x, int y)
            : base(x, y)
        {
            this.initializeMembers();
        }

        private void initializeMembers()
        {
            Sprite = new CheepCheepSprite(this);

            MaxWeight = CheepMaxWeight;
            MinWeight = CheepMinWeight;
            SetRandomWeightAndScaleSpriteToWeight();
            DefaultStrategy = new CasualSwimStrategy();
            SetToDefaultStrategy();
            
        }
    }
}
