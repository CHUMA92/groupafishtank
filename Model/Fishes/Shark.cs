﻿using System.Drawing;
using FishTank.Model.SwimStrategies;
using FishTank.View.Sprites.FishSprites;

namespace FishTank.Model.Fishes
{
    /// <summary>
    ///     Holds information about a shark.
    /// </summary>
    public class Shark : Fish
    {
        private const double SharkMinWeight = 12;
        private const double SharkMaxWeight = 45;
        private const double FowardMovement = 0.90;
        private const double BackwardMovement = 0.03;
        private const double VerticalMovement = 0.035;

        /// <summary>
        /// Initializes a new instance of the <see cref="Shark"/> class.
        /// </summary>
        public Shark()
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shark"/> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        public Shark(Point location)
            : base(location)
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Shark"/> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public Shark(int x, int y)
            : base(x, y)
        {
            this.initializeMembers();
        }

        private void initializeMembers()
        {
            Sprite = new SharkSprite(this);

            MaxWeight = SharkMaxWeight;
            MinWeight = SharkMinWeight;
            MoveFoward = FowardMovement;
            MoveBackward = BackwardMovement;
            MoveUpward = VerticalMovement;
            MoveDownward = VerticalMovement;
            SetRandomWeightAndScaleSpriteToWeight();
            DefaultStrategy = new AggressiveSwimStrategy();
            SetToDefaultStrategy();
        }
    }
}
