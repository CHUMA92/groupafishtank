﻿using System.Drawing;
using FishTank.Model.SwimStrategies;
using FishTank.View.Sprites.FishSprites;

namespace FishTank.Model.Fishes
{
    /// <summary>
    ///     Holds information about a sting shark.
    /// </summary>
    public class StingFish : Fish
    {
        private const double StingMinWeight = .25;
        private const double StingMaxWeight = 2.5;
        private const double FowardMovement = 0.50;
        private const double BackwardMovement = 0.20;
        private const double VerticalMovement = 0.15;

        /// <summary>
        /// Initializes a new instance of the <see cref="StingFish"/> class.
        /// </summary>
        public StingFish()
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StingFish"/> class.
        /// </summary>
        /// <param name="location">Location to create fish</param>
        public StingFish(Point location)
            : base(location)
        {
            this.initializeMembers();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StingFish"/> class.
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public StingFish(int x, int y)
            : base(x, y)
        {
            this.initializeMembers();

        }

        private void initializeMembers()
        {
            Sprite = new StingFishSprite(this);

            MaxWeight = StingMaxWeight;
            MinWeight = StingMinWeight;
            MoveFoward = FowardMovement;
            MoveBackward = BackwardMovement;
            MoveUpward = VerticalMovement;
            MoveDownward = VerticalMovement;
            SetRandomWeightAndScaleSpriteToWeight();
            DefaultStrategy = new RestingSwimStrategy();
            SetToDefaultStrategy();
        }
    }
}