﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using FishTank.Model.Fishes;

namespace FishTank.Model
{
    /// <summary>
    ///     Manages the collection of fish in the tank.
    /// </summary>
    public class FishTankManager
    {
        #region Data members

        private readonly List<Fish> fishes;
        private readonly List<FishFood> food;
        private readonly int height;
        private readonly int width;
        private const int MaxFishTankCanHold = 15;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the maximum capacity of tank.
        /// </summary>
        /// <value>
        /// The maximum capacity of tank.
        /// </value>
        public int MaxCapacityOfTank
        {
            get { return MaxFishTankCanHold; }
        }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        public int Count
        {
            get { return this.fishes.Count; }
        }

        /// <summary>
        /// Gets the fishes.
        /// </summary>
        /// <value>
        /// The fishes.
        /// </value>
        public List<Fish> Fishes
        {
            get { return this.fishes; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Prevents a default instance of the <see cref="FishTankManager" /> class from being created.
        /// </summary>
        private FishTankManager()
        {
            this.fishes = new List<Fish>();
            this.food = new List<FishFood>();
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTankManager" /> class.
        ///     Precondition: None
        /// </summary>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        public FishTankManager(int height, int width)
            : this()
        {
            this.height = height;
            this.width = width;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Places the fish in the tank.
        /// Precondition: None
        /// </summary>
        public void PlaceFishInTank()
        {
            const int numFishes = 3;

            this.createFish(numFishes);
        }

        /// <summary>
        /// Places 3 fish in the tank.
        /// Precondition: None
        /// </summary>
        /// <param name="numFishes">The number fishes.</param>
        public void PlaceFishInTank(int numFishes)
        {
            this.createFish(numFishes);
        }

        /// <summary>
        /// Removes the fish in the tank.
        /// </summary>
        public void RemoveFishInTank()
        {
            this.deleteFish();
            this.removeAllFood();
        }

        /// <summary>
        /// Adds up to five food.
        /// </summary>
        public void AddUpToFiveFood()
        {
            const int minimumFoodToAdd = 3;
            const int maximumFoodToAdd = 5;

            var foodToAdd = RandomGenerator.GetNextInt(minimumFoodToAdd, maximumFoodToAdd + 1);

            for (var i = 0; i < foodToAdd; i++)
            {
                var initialFoodX = RandomGenerator.GetNextInt(50, this.width - 50);
                var initialFoodY = RandomGenerator.GetNextInt(2, 6);

                var addFood = new FishFood(initialFoodX, initialFoodY);
                this.food.Add(addFood);
            }
        }

        /// <summary>
        /// Moves the fish around and the calls the Fish::Paint method to draw the fish.
        /// Precondition: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw on</param>
        /// <exception cref="ArgumentNullException">graphics</exception>
        public void Update(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }

            this.checkEatenFood();

            this.updateFish(graphics);

            this.updateFood(graphics);

            this.checkAllFishCollision();
        }

        /// <summary>
        /// Method that handles all functions involving mouse-click input on Fish objects
        /// </summary>
        /// <param name="mouseX">The mouse x.</param>
        /// <param name="mouseY">The mouse y.</param>
        public void HandleClickedFish(int mouseX, int mouseY)
        {
            var handledAValidFish = false;

            foreach (var fish in Enumerable.Reverse(this.fishes))
            {
                if (checkFishInMouseLocation(mouseX, mouseY, fish))
                {
                    handledAValidFish = this.getClickedFishTypeAndExecuteFunction(fish);
                }

                if (handledAValidFish)
                {
                    break;
                }
            }
        }

        #endregion

        #region Helper Methods

        private void checkEatenFood()
        {
            foreach (var currentFood in Enumerable.Reverse(this.food))
            {
                foreach (var currentFish in this.fishes)
                {

                    this.removeCurrentFood(currentFood, currentFish);
                }
            }
        }

        private void removeCurrentFood(FishFood currentFood, Fish currentFish)
        {
            var isAShark = this.checkIsShark(currentFish);

            if (currentFood.CollisionBox.IntersectsWith(currentFish.CollsionBox) && !isAShark &&
                currentFish.Status != Fish.FishStatus.Dead)
            {
                this.food.Remove(currentFood);
                currentFish.HandleWhenFishEatFood();
                currentFish.SetToDefaultStrategy();
            }
        }

        private void updateFish(Graphics graphics)
        {
            foreach (var fish in this.fishes)
            {
                fish.Update(graphics, this.width, this.height);
            }
        }

        private void updateFood(Graphics graphics)
        {
            foreach (var currentfood in Enumerable.Reverse(this.food))
            {
                currentfood.Update(graphics, this.width, this.height);
                this.deleteFoodPastBoundary(currentfood);
            }
        }

        private void deleteFoodPastBoundary(FishFood checkFood)
        {
            if (checkFood.Y >= this.height)
            {
                this.food.Remove(checkFood);
            }
        }

        private void removeAllFood()
        {
            foreach (var fishFood in Enumerable.Reverse(this.food))
            {
                this.food.Remove(fishFood);
            }
        }

        private static bool checkFishInMouseLocation(int mouseX, int mouseY, Fish fish)
        {
            return (mouseX >= fish.X && mouseX <= fish.X + fish.Width) && (mouseY >= fish.Y && mouseY <= fish.Y + fish.Height);
        }

        private void placeFishWithinBounds(Fish aFish)
        {
            var x = RandomGenerator.GetNextInt(0, (this.width - aFish.Width));
            var y = RandomGenerator.GetNextInt(0, (this.height - aFish.Height));

            aFish.SetFishPositions(x, y);
        }

        private void createFish(int numFishes)
        {
            if (numFishes + this.Count > MaxFishTankCanHold)
            {
                numFishes = MaxFishTankCanHold - this.Count;
            }

            for (var i = 0; i < numFishes; i++)
            {
                var fish = FishFactory.CreateFish();
                this.placeFishWithinBounds(fish);
                this.fishes.Add(fish);

                this.placeFishInFreeSpace(fish);
            }
        }

        private void placeFishInFreeSpace(Fish fish)
        {
            var fishIntersecting = this.checkFishIntersectOnPlacement(fish);

            while (fishIntersecting)
            {
                this.placeFishWithinBounds(fish);
                fishIntersecting = this.checkFishIntersectOnPlacement(fish);
            }
        }

        private bool checkFishIntersectOnPlacement(Fish checkFish)
        {
            var fishIntersects = false;

            foreach (var fish in this.fishes)
            {
                if (fish != checkFish && checkFish.CollsionBox.IntersectsWith(fish.CollsionBox))
                {
                    fishIntersects = true;
                }
            }

            return fishIntersects;
        }

        private void checkAllFishCollision()
        {
            foreach (var currentFish in this.fishes)
            {
                foreach (var otherFish in this.fishes)
                {
                    this.handleFishCollision(currentFish, otherFish);
                }
            }
        }

        private void handleFishCollision(Fish currentFish, Fish otherFish)
        {
            var fishIntersect = checkTwoFishColliding(currentFish, otherFish);

            if (fishIntersect)
            {
                this.getCollidingFishTypesAndHandleCollision(currentFish, otherFish);
            }
        }

        private static bool checkTwoFishColliding(Fish firstFist, Fish secondFish)
        {
            var fishIntersecting = firstFist != secondFish && firstFist.CollsionBox.IntersectsWith(secondFish.CollsionBox);

            return fishIntersecting;
        }

        private void getCollidingFishTypesAndHandleCollision(Fish currentFish, Fish otherFish)
        {
            if (this.checkIsShark(currentFish) && this.checkIsShark(otherFish))
            {
                this.handleSharkCollidingWithShark(currentFish, otherFish);
            }
            else if (this.checkIsShark(otherFish))
            {
                this.handleFishCollidingWithShark(currentFish, otherFish);
            }
        }

        private bool checkIsShark(Fish checkFish)
        {
            return (checkFish.GetType() == typeof(Shark) || checkFish.GetType() == typeof(TigerShark));
        }

        private void handleFishCollidingWithShark(Fish currentFish, Fish otherFish)
        {
            if (currentFish.Status != Fish.FishStatus.Dead && otherFish.CheckSharkCanBite())
            {
                currentFish.HandleWhenSharkBitesFish();
                otherFish.HandleWhenSharkBitesFish();
            }
        }

        private void handleSharkCollidingWithShark(Fish currentShark, Fish otherShark)
        {
            var firstSharkNotTiredOrResting = this.checkIfSharkIsRestingOrTired(currentShark);
            var secondSharkNotTiredOrResting = this.checkIfSharkIsRestingOrTired(otherShark);

            if (firstSharkNotTiredOrResting && secondSharkNotTiredOrResting)
            {
                handleWhenSharkAreInOppositeDirections(currentShark, otherShark);
            }

        }

        private static void handleWhenSharkAreInOppositeDirections(Fish currentShark, Fish otherShark)
        {
            if (currentShark.FacingDirection == Fish.Direction.Right &&
                otherShark.FacingDirection == Fish.Direction.Left)
            {
                handleSharkAreHeadToHeadFromTheRightPosition(currentShark, otherShark);
            }
            else if (currentShark.FacingDirection == Fish.Direction.Left &&
                     otherShark.FacingDirection == Fish.Direction.Right)
            {
                handleSharkAreHeadToHeadFromTheLeftPosition(currentShark, otherShark);
            }
        }

        private static void handleSharkAreHeadToHeadFromTheLeftPosition(Fish currentShark, Fish otherShark)
        {
            if (currentShark.X > otherShark.X)
            {
                currentShark.ChangeDirection();
                otherShark.ChangeDirection();
            }
        }

        private static void handleSharkAreHeadToHeadFromTheRightPosition(Fish currentShark, Fish otherShark)
        {
            if (currentShark.X < otherShark.X)
            {
                currentShark.ChangeDirection();
                otherShark.ChangeDirection();
            }
        }

        private bool checkIfSharkIsRestingOrTired(Fish shark)
        {
            var neitherRestingNorTired = shark.Status != Fish.FishStatus.Resting && shark.Status != Fish.FishStatus.Tired;

            return neitherRestingNorTired;
        }

        private void deleteFish()
        {
            this.fishes.Clear();
        }

        private void deleteFish(Fish aFish)
        {
            this.fishes.Remove(aFish);
        }

        private bool getClickedFishTypeAndExecuteFunction(Fish clickedFish)
        {
            bool handledValidFish;

            if (clickedFish.GetType() != typeof(Shark) && clickedFish.GetType() != typeof(TigerShark))
            {
                handledValidFish = this.scoopDeadFish(clickedFish);
            }
            else
            {
                handledValidFish = this.wakeSharkIfResting(clickedFish);
            }

            return handledValidFish;
        }

        private bool wakeSharkIfResting(Fish clickedShark)
        {
            var wokeRestingShark = false;

            if (clickedShark.Status == Fish.FishStatus.Resting)
            {
                clickedShark.WakeSharkFromResting(this.height);
                wokeRestingShark = true;
            }

            return wokeRestingShark;
        }

        private bool scoopDeadFish(Fish clickedFish)
        {
            var scoopedFish = false;

            if (clickedFish.Status == Fish.FishStatus.Dead)
            {
                this.deleteFish(clickedFish);
                scoopedFish = true;
            }

            return scoopedFish;
        }

        #endregion
    }
}