﻿using System;
using FishTank.Model.Fishes;

namespace FishTank.Model
{
    /// <summary>
    /// Creates a direction
    /// </summary>
    public static class FishDirection
    {
        #region Data members

        /// <summary>
        /// 
        /// </summary>
        private enum Direction
        {
            Backward,
            Foward,
            Up,
            Down

        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the direction.
        /// </summary>
        /// <returns></returns>
        public static void SetTheDirection(Fish fishDeterminingDirection)
        {
            var direction = getRandomDirection();

            switch (direction)
            {
                case Direction.Backward:
                    if (fishDeterminingDirection.DirectionFacingRight)
                    {
                        fishDeterminingDirection.X -= fishDeterminingDirection.SpeedX; 
                    }
                    else
                    {
                        fishDeterminingDirection.X += fishDeterminingDirection.SpeedX; 
                    }
                    break;
                case Direction.Foward:
                    if (fishDeterminingDirection.DirectionFacingRight)
                    {
                        fishDeterminingDirection.X += fishDeterminingDirection.SpeedX;
                    }
                    else
                    {
                        fishDeterminingDirection.X -= fishDeterminingDirection.SpeedX;
                    }
                    break;
                case Direction.Down:
                    if (fishDeterminingDirection.DirectionFacingRight)
                    {
                        fishDeterminingDirection.Y += fishDeterminingDirection.SpeedY;
                    }
                    else
                    {
                        fishDeterminingDirection.Y -= fishDeterminingDirection.SpeedY;
                    }
                    break;
                case Direction.Up:
                    if (fishDeterminingDirection.DirectionFacingRight)
                    {
                        fishDeterminingDirection.Y -= fishDeterminingDirection.SpeedY;
                    }
                    else
                    {
                        fishDeterminingDirection.Y += fishDeterminingDirection.SpeedY;
                    }
                    break;
            }
        }

        /// <summary>
        /// Gets the random direction.
        /// </summary>
        /// <returns></returns>
        private static Direction getRandomDirection()
        {
            var directions = (Direction[])Enum.GetValues(typeof(Direction));
            var randomDirection = directions[RandomGenerator.GetNextInt(directions.Length)];
            return randomDirection;
        }

        #endregion
    }
}
