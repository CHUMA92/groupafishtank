﻿using System;

namespace FishTank.Model
{
    /// <summary>
    /// Class to hold the random static utility methods.
    /// </summary>
    public class RandomGenerator
    {
        #region Data memebers

        private static readonly Random Randomizer = new Random(); 

        #endregion

        #region Methods

        /// <summary>
        /// Randomizes the integer number.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>Random number.</returns>
        public static int GetNextInt(int value)
        {
            return Randomizer.Next(value);
        }

        /// <summary>
        /// Randomizes the numbers inclusively integer numbers from min to max value.
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <returns>Random number.</returns>
        public static int GetNextInt(int min, int max)
        {
            return Randomizer.Next(min, max);
        }

        /// <summary>
        /// Randomizes the double number.
        /// </summary>
        /// <returns>
        /// Random number.
        /// </returns>
        public static double GetNextDouble()
        {
            return Randomizer.NextDouble();
        }

        /// <summary>
        /// Randomizes the number inclusively double numbers from min to max value.
        /// </summary>
        /// <param name="min">The minimum.</param>
        /// <param name="max">The maximum.</param>
        /// <returns>Random number.</returns>
        public static double GetNextDouble(double min, double max)
        {
            return Randomizer.NextDouble() * (max - min) + min;
        }

        #endregion
    }
}