﻿using FishTank.Model.Fishes;
using FishTank.Model.SwimStrategies;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FishTank.Tests
{
    /// <summary>
    ///     Tests the fish movement strategies
    /// </summary>
    [TestClass]
    public class FishTankMovementTest
    {
        private const int InitialPlacement = 50;
        private const int CanvasSize = 100;

        /// <summary>
        /// Tests the rest movement.
        /// </summary>
        [TestMethod]
        public void TestRestMovement()
        {
            var shark = new Shark();
            shark.SetStrategy(new RestingSwimStrategy());
            shark.X = InitialPlacement;
            shark.Y = InitialPlacement;
            shark.Move(CanvasSize, CanvasSize);
            const int maximumMovement = InitialPlacement + 4;
            const int minimumMovement = InitialPlacement - 4;

            Assert.AreEqual(true, shark.X <= maximumMovement);
            Assert.AreEqual(true, shark.Y <= maximumMovement);
            Assert.AreEqual(true, shark.X >= minimumMovement);
            Assert.AreEqual(true, shark.Y >= minimumMovement);
        }

        /// <summary>
        /// Tests the casual swim movement.
        /// </summary>
        [TestMethod]
        public void TestCasualSwimMovement()
        {
            var shark = new Shark();
            shark.SetStrategy(new CasualSwimStrategy());

            shark.X = InitialPlacement;
            shark.Y = InitialPlacement;
            shark.Move(CanvasSize, CanvasSize);


            switch (shark.FacingDirection)
            {
                case Fish.Direction.Right:
                    Assert.AreEqual(true, shark.X > InitialPlacement);
                    break;
                case Fish.Direction.Left:
                    Assert.AreEqual(true, shark.X < InitialPlacement);
                    break;
                default:
                    assertIllegalFacingState();
                    break;
            }
        }

        /// <summary>
        /// Tests the aggressive swim movement.
        /// </summary>
        [TestMethod]
        public void TestAggressiveSwimMovement()
        {
            var shark = new Shark();
            shark.SetStrategy(new AggressiveSwimStrategy());

            shark.X = InitialPlacement;
            shark.Y = InitialPlacement;
            shark.Move(CanvasSize, CanvasSize);


            switch (shark.FacingDirection)
            {
                case Fish.Direction.Right:
                    Assert.AreEqual(true, shark.X > InitialPlacement);
                    break;
                case Fish.Direction.Left:
                    Assert.AreEqual(true, shark.X < InitialPlacement);
                    break;
                default:
                    assertIllegalFacingState();
                    break;
            }
        }

        /// <summary>
        /// Tests the swim to top movement.
        /// </summary>
        [TestMethod]
        public void TestSwimToTopMovement()
        {
            var shark = new Shark();
            shark.SetStrategy(new SwimToTopStrategy());

            shark.X = InitialPlacement;
            shark.Y = InitialPlacement;
            shark.Move(CanvasSize, CanvasSize);


           Assert.AreEqual(true, shark.Y < InitialPlacement);
        }

        /// <summary>
        /// Tests the swim to bottom movement.
        /// </summary>
        [TestMethod]
        public void TestSwimToBottomMovement()
        {
            var shark = new Shark();
            shark.SetStrategy(new SwimToBottomStrategy());

            shark.X = InitialPlacement;
            shark.Y = InitialPlacement;
            shark.Move(CanvasSize, CanvasSize);


            Assert.AreEqual(true, shark.Y > InitialPlacement);
        }

        private static void assertIllegalFacingState()
        {
            Assert.Fail("Fish was in an illegal facing state");
        }
    }
}