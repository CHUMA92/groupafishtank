using System;
using System.Windows.Forms;
using FishTank.Model;

namespace FishTank.View.Forms
{
    /// <summary>
    ///     Manages the form that displays the fish tank.
    /// </summary>
    public partial class FishTank
    {
        #region Data members

        private readonly FishTankManager fishTankManager;
        private bool sortByName = true;
        private bool sortByWeight = true;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="FishTank" /> class.
        ///     Precondition: None
        /// </summary>
        public FishTank()
        {
            this.InitializeComponent();

            this.fishTankManager = new FishTankManager(this.fishTankPictureBox.Height, this.fishTankPictureBox.Width);

            this.animationTimer.Start();
            SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            this.updateLabel();

            this.dgvFish.ColumnCount = 2;
            this.dgvFish.Columns[0].Name = "Weight";
            this.dgvFish.Columns[1].Name = "Type";
            this.updateDataTable();
        }

        #endregion

        #region Event generated methods

        /// <summary>
        /// Handles the Tick event of the animationTimer control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void animationTimer_Tick(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Handles the Paint event of the fishTankCanvasPictureBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PaintEventArgs"/> instance containing the event data.</param>
        private void fishTankCanvasPictureBox_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            this.fishTankManager.Update(g);
        }

        /// <summary>
        /// Handles the Click event of the btnAddFish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnAddFish_Click(object sender, EventArgs e)
        {
            var numOfFish = this.getFishesPlacedInTank();

            this.enableClearAndFeedButton(numOfFish);
            this.txtNumOfFish.Focus();
            this.updateDataTable();

            this.updateLabel();
            this.enableOrDisableAddButtonAndTextBoxDependingOnCount();
        }

        /// <summary>
        /// Handles the Click event of the btnClearFish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnClearFish_Click(object sender, EventArgs e)
        {
            this.fishTankManager.RemoveFishInTank();
            this.txtNumOfFish.Clear();
            this.txtNumOfFish.Text = @"0";
            this.dgvFish.Rows.Clear();

            if (!this.btnClearFish.CausesValidation)
            {
                return;
            }
            this.btnClearFish.Enabled = false;
            this.btnFeed.Enabled = false;
            this.txtNumOfFish.Focus();
            this.updateLabel();
            this.enableOrDisableAddButtonAndTextBoxDependingOnCount();
        }

        /// <summary>
        /// Handles the KeyPress event of the txtNumOfFish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="KeyPressEventArgs"/> instance containing the event data.</param>
        private void txtNumOfFish_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the MouseClick event of the fishTankPictureBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void fishTankPictureBox_MouseClick(object sender, EventArgs e)
        {
            var mousePoint = this.fishTankPictureBox.PointToClient(MousePosition);
            this.fishTankManager.HandleClickedFish(mousePoint.X, mousePoint.Y);
            this.updateLabel();
            this.enableOrDisableAddButtonAndTextBoxDependingOnCount();
            this.updateDataTable();
        }

        /// <summary>
        /// Handles the Click event of the btnFeed control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void btnFeed_Click(object sender, EventArgs e)
        {
            this.fishTankManager.AddUpToFiveFood();
            this.updateDataTable();
        }

        /// <summary>
        /// Handles the ColumnHeaderMouseClick event of the dgvFish control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellMouseEventArgs"/> instance containing the event data.</param>
        private void dgvFish_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                this.toggleSortWeight();
            }
            else
            {
                this.toggleSortName();
            }
        }

        private void toggleSortWeight()
        {
            if (this.sortByWeight)
            {
                this.sortByWeight = false;
                this.dgvFish.Columns[0].HeaderCell.SortGlyphDirection = SortOrder.None;
            }
            else
            {
                this.sortByWeight = true;
                this.dgvFish.Columns[0].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        private void toggleSortName()
        {
            if (this.sortByName)
            {
                this.sortByName = false;
                this.dgvFish.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.None;
            }
            else
            {
                this.sortByName = true;
                this.dgvFish.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
            }
        }

        #endregion

        #region Helper methods

        private void addFishRowsToDataTable()
        {
            foreach (var fish in this.fishTankManager.Fishes)
            {
                this.dgvFish.Rows.Add(fish.Weight.ToString("#0.000"), fish.GetType().Name);
            }
        }

        private void updateDataTable()
        {
            this.dgvFish.Rows.Clear();
            this.addFishRowsToDataTable();
        }

        private void enableClearAndFeedButton(int numOfFish)
        {
            if (numOfFish > 0)
            {
                this.btnClearFish.Enabled = true;
                this.btnFeed.Enabled = true;
            }
        }

        private int getFishesPlacedInTank()
        {
            var numOfFish = Convert.ToInt32(this.txtNumOfFish.Text);
            this.fishTankManager.PlaceFishInTank(numOfFish);
            return numOfFish;
        }

        private void updateLabel()
        {
            this.FishCapacityLabel.Text = this.fishTankManager.Count + @" / " + this.fishTankManager.MaxCapacityOfTank;
        }


        private void enableOrDisableAddButtonAndTextBoxDependingOnCount()
        {
            if (this.fishTankManager.Count != this.fishTankManager.MaxCapacityOfTank)
            {
                this.txtNumOfFish.Enabled = true;
                this.btnAddFish.Enabled = true;
            }
            else
            {
                this.txtNumOfFish.Enabled = false;
                this.btnAddFish.Enabled = false;
                this.txtNumOfFish.Text = "" + 15;
            }
        }

        #endregion
    }
}
