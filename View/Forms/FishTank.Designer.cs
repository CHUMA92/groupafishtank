using System.ComponentModel;
using System.Windows.Forms;

namespace FishTank.View.Forms
{
    partial class FishTank : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FishTank));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.fishTankPictureBox = new System.Windows.Forms.PictureBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.txtNumOfFish = new System.Windows.Forms.TextBox();
            this.btnAddFish = new System.Windows.Forms.Button();
            this.lblNumOfFish = new System.Windows.Forms.Label();
            this.btnClearFish = new System.Windows.Forms.Button();
            this.btnFeed = new System.Windows.Forms.Button();
            this.FishCapacityLabel = new System.Windows.Forms.Label();
            this.dgvFish = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.fishTankPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFish)).BeginInit();
            this.SuspendLayout();
            // 
            // fishTankPictureBox
            // 
            this.fishTankPictureBox.BackColor = System.Drawing.Color.MediumBlue;
            this.fishTankPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fishTankPictureBox.BackgroundImage")));
            this.fishTankPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fishTankPictureBox.Location = new System.Drawing.Point(12, 12);
            this.fishTankPictureBox.Name = "fishTankPictureBox";
            this.fishTankPictureBox.Size = new System.Drawing.Size(700, 450);
            this.fishTankPictureBox.TabIndex = 0;
            this.fishTankPictureBox.TabStop = false;
            this.fishTankPictureBox.Click += new System.EventHandler(this.fishTankPictureBox_MouseClick);
            this.fishTankPictureBox.Paint += new System.Windows.Forms.PaintEventHandler(this.fishTankCanvasPictureBox_Paint);
            // 
            // animationTimer
            // 
            this.animationTimer.Interval = 200;
            this.animationTimer.Tick += new System.EventHandler(this.animationTimer_Tick);
            // 
            // txtNumOfFish
            // 
            this.txtNumOfFish.Location = new System.Drawing.Point(198, 473);
            this.txtNumOfFish.Name = "txtNumOfFish";
            this.txtNumOfFish.Size = new System.Drawing.Size(59, 20);
            this.txtNumOfFish.TabIndex = 1;
            this.txtNumOfFish.Text = "0";
            this.txtNumOfFish.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumOfFish_KeyPress);
            // 
            // btnAddFish
            // 
            this.btnAddFish.Location = new System.Drawing.Point(345, 470);
            this.btnAddFish.Name = "btnAddFish";
            this.btnAddFish.Size = new System.Drawing.Size(75, 23);
            this.btnAddFish.TabIndex = 2;
            this.btnAddFish.Text = "Add Fish";
            this.btnAddFish.UseVisualStyleBackColor = true;
            this.btnAddFish.Click += new System.EventHandler(this.btnAddFish_Click);
            // 
            // lblNumOfFish
            // 
            this.lblNumOfFish.AutoSize = true;
            this.lblNumOfFish.Location = new System.Drawing.Point(13, 476);
            this.lblNumOfFish.Name = "lblNumOfFish";
            this.lblNumOfFish.Size = new System.Drawing.Size(180, 13);
            this.lblNumOfFish.TabIndex = 3;
            this.lblNumOfFish.Text = "Enter number of fish to place in tank:";
            // 
            // btnClearFish
            // 
            this.btnClearFish.Enabled = false;
            this.btnClearFish.Location = new System.Drawing.Point(426, 470);
            this.btnClearFish.Name = "btnClearFish";
            this.btnClearFish.Size = new System.Drawing.Size(75, 23);
            this.btnClearFish.TabIndex = 4;
            this.btnClearFish.Text = "Clear Fish";
            this.btnClearFish.UseVisualStyleBackColor = true;
            this.btnClearFish.Click += new System.EventHandler(this.btnClearFish_Click);
            // 
            // btnFeed
            // 
            this.btnFeed.Location = new System.Drawing.Point(507, 470);
            this.btnFeed.Name = "btnFeed";
            this.btnFeed.Size = new System.Drawing.Size(75, 23);
            this.btnFeed.TabIndex = 10;
            this.btnFeed.Text = "Feed";
            this.btnFeed.UseVisualStyleBackColor = true;
            this.btnFeed.Click += new System.EventHandler(this.btnFeed_Click);
            // 
            // FishCapacityLabel
            // 
            this.FishCapacityLabel.AutoSize = true;
            this.FishCapacityLabel.Location = new System.Drawing.Point(263, 475);
            this.FishCapacityLabel.Name = "FishCapacityLabel";
            this.FishCapacityLabel.Size = new System.Drawing.Size(24, 13);
            this.FishCapacityLabel.TabIndex = 11;
            this.FishCapacityLabel.Text = "0/0";
            this.FishCapacityLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // dgvFish
            // 
            this.dgvFish.AllowUserToAddRows = false;
            this.dgvFish.AllowUserToDeleteRows = false;
            this.dgvFish.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvFish.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Lucida Bright", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFish.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvFish.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFish.Location = new System.Drawing.Point(718, 12);
            this.dgvFish.MultiSelect = false;
            this.dgvFish.Name = "dgvFish";
            this.dgvFish.ReadOnly = true;
            this.dgvFish.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgvFish.RowHeadersVisible = false;
            this.dgvFish.Size = new System.Drawing.Size(200, 450);
            this.dgvFish.TabIndex = 12;
            this.dgvFish.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvFish_ColumnHeaderMouseClick);
            // 
            // FishTank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 516);
            this.Controls.Add(this.dgvFish);
            this.Controls.Add(this.FishCapacityLabel);
            this.Controls.Add(this.btnFeed);
            this.Controls.Add(this.btnClearFish);
            this.Controls.Add(this.lblNumOfFish);
            this.Controls.Add(this.btnAddFish);
            this.Controls.Add(this.txtNumOfFish);
            this.Controls.Add(this.fishTankPictureBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FishTank";
            this.Text = "FishTank A5 by Chuma Okafor, Shledon Hunter";
            ((System.ComponentModel.ISupportInitialize)(this.fishTankPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFish)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private PictureBox fishTankPictureBox;
        private Timer animationTimer;
        private TextBox txtNumOfFish;
        private Button btnAddFish;
        private Label lblNumOfFish;
        private Button btnClearFish;
        private Button btnFeed;
        private Label FishCapacityLabel;
        private DataGridView dgvFish;
    }
}

