﻿using System;
using System.Drawing;
using System.Linq;
using FishTank.Model.Fishes;

namespace FishTank.View.Sprites.FishSprites
{
    /// <summary>
    /// Responsible for drawing the sting fish sprite on the screen.
    /// </summary>
    public class StingFishSprite : FishSprite
    {
        #region Instance variables

        private const int Girth = 150;
        private const int Stature = 70;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the body coordinates.
        /// </summary>
        /// <value>
        /// The body coordinates.
        /// </value>
        private Point[] BodyCoordinates
        {
            get
            {
                var bodyCoordinates = new[]
                {
                    new Point {X = 0, Y = 5},
                    new Point {X = 30, Y = 15},
                    new Point {X = 40, Y = 10},
                    new Point {X = 50, Y = 7},
                    new Point {X = 60, Y = 0},
                    new Point {X = 70, Y = 7},
                    new Point {X = 90, Y = 12},
                    new Point {X = 100, Y = 15},
                    new Point {X = 115, Y = 22},
                    new Point {X = Girth, Y = 25},
                    new Point {X = 60, Y = Stature}
                };
                return bodyCoordinates;
            }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        protected int TheWidth
        {
            get { return Girth; }
        }


        /// <summary>
        /// Gets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        protected int TheHeight
        {
            get { return Stature; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="StingFishSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        public StingFishSprite(Fish fish)
            : base(fish, Girth, Stature)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Draws a fish
        /// Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        public override void Paint(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }

            DrawFish(graphics);
            this.DrawEye(graphics);
        }

        /// <summary>
        /// Draws the body.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="offsetX">The offset x.</param>
        /// <param name="offsetY">The offset y.</param>
        protected override void DrawBody(Graphics graphics, int offsetX, int offsetY)
        {
            Point[] adjustedCoordinates;
            var theBrush = this.changeColorWhenBitten(out adjustedCoordinates);

            ApplyScaleToImagePoints(adjustedCoordinates);

            if (TheFish.Status == Fish.FishStatus.Dead)
            {
                adjustedCoordinates =
                        adjustedCoordinates.Select(point => new Point(point.X, (AdjustedHeight - point.Y))).ToArray();
            }

            adjustedCoordinates = this.drawBodyFacingDirection(offsetX, offsetY, adjustedCoordinates);

            graphics.FillClosedCurve(theBrush, adjustedCoordinates);
        }

        /// <summary>
        /// Draws the eye.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        protected void DrawEye(Graphics graphics)
        {
            const int xIncrement = 95;
            const int yIncrement = 17;
            const int ellipseWidth = 5;
            const int ellipseHeight = 5;

            if (TheFish.Status != Fish.FishStatus.Dead)
            {
                this.drawEyeWhileAlive(graphics, xIncrement, yIncrement, ellipseWidth, ellipseHeight);
            }
            else
            {
                this.drawEyeWhileDead(graphics, xIncrement, yIncrement, ellipseWidth, ellipseHeight);
            }
        }

        private void drawEyeWhileAlive(Graphics graphics, int xIncrement, int yIncrement, int ellipseWidth,
           int ellipseHeight)
        {

            var redBrush = new SolidBrush(Color.Red);

            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    graphics.FillEllipse(redBrush, (xIncrement * SizePercentage) + X, (yIncrement * SizePercentage) + Y,
                        ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                case Fish.Direction.Left:
                    graphics.FillEllipse(redBrush, (AdjustedWidth - (xIncrement * SizePercentage)) + X,
                        (yIncrement * SizePercentage) + Y, ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void drawEyeWhileDead(Graphics graphics, int xIncrement, int yIncrement, int ellipseWidth, int ellipseHeight)
        {
            var grayBrush = new SolidBrush(Color.DarkSlateGray);

            const int yErrorOffset = 4;

            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    graphics.FillEllipse(grayBrush, (xIncrement * SizePercentage) + X,
                        (AdjustedHeight - ((yIncrement + yErrorOffset) * SizePercentage)) + Y,
                        ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                case Fish.Direction.Left:
                    graphics.FillEllipse(grayBrush, (AdjustedWidth - (xIncrement * SizePercentage)) + X,
                        (AdjustedHeight - ((yIncrement + yErrorOffset) * SizePercentage)) + Y, ellipseWidth * SizePercentage,
                        ellipseHeight * SizePercentage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private Point[] drawBodyFacingDirection(int offsetX, int offsetY, Point[] adjustedCoordinates)
        {
            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    adjustedCoordinates =
                        adjustedCoordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();
                    break;
                case Fish.Direction.Left:
                    adjustedCoordinates =
                        adjustedCoordinates.Select(point => new Point(AdjustedWidth - point.X + offsetX, point.Y + offsetY))
                                           .ToArray();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return adjustedCoordinates;
        }

        private SolidBrush changeColorWhenBitten(out Point[] adjustedCoordinates)
        {
            SolidBrush theBrush;

            adjustedCoordinates = this.BodyCoordinates;
            switch (TheFish.BiteCount)
            {
                case 0:
                    theBrush = new SolidBrush(Color.MidnightBlue);
                    break;
                case 1:
                    theBrush = new SolidBrush(Color.Purple);
                    break;
                case 2:
                    theBrush = new SolidBrush(Color.MediumBlue);
                    break;
                default:
                    theBrush = new SolidBrush(Color.Aqua);
                    break;
            }
            return theBrush;
        }

        #endregion
    }
}