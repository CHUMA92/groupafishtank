﻿using System;
using System.Drawing;
using FishTank.Model.Fishes;

namespace FishTank.View.Sprites.FishSprites
{
    /// <summary>
    /// Responsible for drawing the spotted sting fish sprite on the screen.
    /// </summary>
    public class SpottedStingFishSprite : StingFishSprite
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpottedStingFishSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        public SpottedStingFishSprite(Fish fish)
            : base(fish)
        {
        }

        /// <summary>
        /// Draws a fish
        /// Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);
            this.drawDot(graphics);
        }

        private void drawDot(Graphics graphics)
        {
            var whiteBrush = new SolidBrush(Color.White);
            const int xIncrement = 75;
            const int yIncrement = 35;
            const int ellipseWidth = 10;
            const int ellipseHeight = 10;

            if (TheFish.Status != Fish.FishStatus.Dead)
            {
                this.drawDotWhileAlive(graphics, whiteBrush, xIncrement, yIncrement, ellipseWidth, ellipseHeight);
            }
            else
            {
                this.drawDotWhileDead(graphics, whiteBrush, xIncrement, yIncrement, ellipseWidth, ellipseHeight);
            }
        }

        private void drawDotWhileAlive(Graphics graphics, SolidBrush whiteBrush, int xIncrement, int yIncrement, int ellipseWidth,
            int ellipseHeight)
        {
            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    graphics.FillEllipse(whiteBrush, (xIncrement * SizePercentage) + X, (yIncrement * SizePercentage) + Y,
                        ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                case Fish.Direction.Left:
                    graphics.FillEllipse(whiteBrush, (AdjustedWidth - (xIncrement * SizePercentage)) + X,
                        (yIncrement * SizePercentage) + Y, ellipseWidth * SizePercentage,
                        ellipseHeight * SizePercentage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void drawDotWhileDead(Graphics graphics, Brush whiteBrush, int xIncrement, int yIncrement,
            int ellipseWidth, int ellipseHeight)
        {

            const int yErrorOffset = 4;

            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    graphics.FillEllipse(whiteBrush, (xIncrement * SizePercentage) + X, (AdjustedHeight - ((yIncrement + yErrorOffset) * SizePercentage)) + Y,
                        ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                case Fish.Direction.Left:
                    graphics.FillEllipse(whiteBrush, (AdjustedWidth - (xIncrement * SizePercentage)) + X,
                    (AdjustedHeight - ((yIncrement + yErrorOffset) * SizePercentage)) + Y, ellipseWidth * SizePercentage,
                    ellipseHeight * SizePercentage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
