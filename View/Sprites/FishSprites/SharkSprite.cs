﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using FishTank.Model.Fishes;

namespace FishTank.View.Sprites.FishSprites
{
    /// <summary>
    ///     Responsible for drawing the shark sprite on the screen.
    /// </summary>
    public class SharkSprite : FishSprite
    {
        #region Data members

        private const int Girth = 130;
        private const int Stature = 50;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the body coordinates.
        /// </summary>
        /// <value>
        /// The body coordinates.
        /// </value>
        private Point[] BodyCoordinates
        {
            get
            {
                var bodyCoordinates = new[]
                {
                    new Point {X = 0, Y = 5},
                    new Point {X = 30, Y = 15},
                    new Point {X = 40, Y = 10},
                    new Point {X = 50, Y = 7},
                    new Point {X = 60, Y = 0},
                    new Point {X = 70, Y = 7},
                    new Point {X = 90, Y = 12},
                    new Point {X = 100, Y = 15},
                    new Point {X = 115, Y = 22},
                    new Point {X = Girth, Y = 25},
                    new Point {X = 115, Y = 28},
                    new Point {X = 100, Y = 35},
                    new Point {X = 90, Y = 40},
                    new Point {X = 70, Y = 43},
                    new Point {X = 60, Y = Stature},
                    new Point {X = 50, Y = 43},
                    new Point {X = 40, Y = 40},
                    new Point {X = 30, Y = 35},
                    new Point {X = 0, Y = 45},
                    new Point {X = 15, Y = 25}
                };
                return bodyCoordinates;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SharkSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        public SharkSprite(Fish fish)
            : base(fish, Girth, Stature)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Draws a fish
        /// Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        public override void Paint(Graphics graphics)
        {
            if (graphics == null)
            {
                throw new ArgumentNullException("graphics");
            }

            DrawFish(graphics);
            this.DrawEye(graphics);
        }

        /// <summary>
        /// Draws the body.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="offsetX">The offset x.</param>
        /// <param name="offsetY">The offset y.</param>
        protected override void DrawBody(Graphics graphics, int offsetX, int offsetY)
        {
            var blackBrush = new SolidBrush(Color.Black);
            var adjustedCoordinates = this.BodyCoordinates;

            ApplyScaleToImagePoints(adjustedCoordinates);

            this.drawBodyInFacingDirection(graphics, offsetX, offsetY, adjustedCoordinates, blackBrush);
        }

        /// <summary>
        /// Draws the eye.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        protected void DrawEye(Graphics graphics)
        {
            var redBrush = new SolidBrush(Color.Red);
            const int xIncrement = 95;
            const int yIncrement = 17;
            const int ellipseWidth = 5;
            const int ellipseHeight = 5;

            this.drawEyeInFacingDirection(graphics, redBrush, xIncrement, yIncrement, ellipseWidth, ellipseHeight);
        }

        private void drawBodyInFacingDirection(Graphics graphics, int offsetX, int offsetY, Point[] adjustedCoordinates,
            Brush blackBrush)
        {
            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    adjustedCoordinates =
                        adjustedCoordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();

                    graphics.FillPolygon(blackBrush, adjustedCoordinates);
                    break;
                case Fish.Direction.Left:
                    adjustedCoordinates =
                        adjustedCoordinates.Select(point => new Point(AdjustedWidth - point.X + offsetX, point.Y + offsetY))
                                           .ToArray();

                    graphics.FillPolygon(blackBrush, adjustedCoordinates);
                    graphics.TransformPoints(CoordinateSpace.World, CoordinateSpace.Page, adjustedCoordinates);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void drawEyeInFacingDirection(Graphics graphics, SolidBrush redBrush, int xIncrement, int yIncrement,
            int ellipseWidth, int ellipseHeight)
        {
            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    graphics.FillEllipse(redBrush, (xIncrement * SizePercentage) + X, (yIncrement * SizePercentage) + Y,
                        ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                case Fish.Direction.Left:
                    graphics.FillEllipse(redBrush, (AdjustedWidth - (xIncrement * SizePercentage)) + X,
                        (yIncrement * SizePercentage) + Y, ellipseWidth * SizePercentage, ellipseHeight * SizePercentage);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}