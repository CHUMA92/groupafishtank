﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using FishTank.Model.Fishes;
using FishTank.Properties;

namespace FishTank.View.Sprites.FishSprites
    {
        /// <summary>
        /// Responsible for drawing the cheep cheep sprite on the screen.
        /// </summary>
        public class CheepCheepSprite : FishSprite
        {
            #region Data members

            private readonly Image cheepcheepSprite = Resources.CheepCheepSpriteRight;

            #endregion

            private Image Sprite
            {
                get { return this.cheepcheepSprite; }
            }

            #region Constructors

            /// <summary>
            /// Initializes a new instance of the <see cref="SharkSprite"/> class.
            /// </summary>
            /// <param name="fish">The fish.</param>
            public CheepCheepSprite(Fish fish)
                : base(fish)
            {
                Width = this.cheepcheepSprite.Width;
                Height = this.cheepcheepSprite.Height;
            }

            #endregion

            #region Methods

            /// <summary>
            /// Draws a fish
            /// Preconditon: graphics != null
            /// </summary>
            /// <param name="graphics">The graphics object to draw the fish one</param>
            public override void Paint(Graphics graphics)
            {
                if (graphics == null)
                {
                    throw new ArgumentNullException("graphics");
                }

                DrawFish(graphics);
            }

            /// <summary>
            /// Draws the body.
            /// </summary>
            /// <param name="graphics">The graphics.</param>
            /// <param name="offsetX">The offset x.</param>
            /// <param name="offsetY">The offset y.</param>
            protected override void DrawBody(Graphics graphics, int offsetX, int offsetY)
            {
                if (graphics == null)
                {
                    throw new ArgumentNullException("graphics");
                }

                if (TheFish.Status != Fish.FishStatus.Dead)
                {
                    this.drawSpriteAlive(graphics, offsetX, offsetY);
                }
                else
                {
                    this.drawSpriteDead(graphics, offsetX, offsetY);
                }
            }

            private void drawSpriteAlive(Graphics graphics, int offsetX, int offsetY)
            {

                var drawImage = this.getImageBasedOnBiteCount();

                this.drawSprite(graphics, offsetX, offsetY, drawImage);
            }

            private void drawSpriteDead(Graphics graphics, int offsetX, int offsetY)
            {
                var drawImage = this.getImageBasedOnBiteCount();
                drawImage.RotateFlip(RotateFlipType.RotateNoneFlipY);

                this.drawSprite(graphics, offsetX, offsetY, drawImage);
            }

            private void drawSprite(Graphics graphics, int offsetX, int offsetY, Image drawImage)
            {
                switch (TheFish.FacingDirection)
                {
                    case Fish.Direction.Right:
                        graphics.DrawImage(drawImage, offsetX, offsetY, AdjustedWidth, AdjustedHeight);
                        break;
                    case Fish.Direction.Left:

                        drawImage.RotateFlip(RotateFlipType.RotateNoneFlipX);

                        graphics.DrawImage(drawImage, offsetX, offsetY, AdjustedWidth, AdjustedHeight);

                        drawImage.RotateFlip(RotateFlipType.RotateNoneFlipX);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            private Image getImageBasedOnBiteCount()
            {
                Image fishImage;

                switch (TheFish.BiteCount)
                {
                    case 0:
                        fishImage = this.Sprite;
                        break;
                    case 1:
                        fishImage = this.returnBluedImage(this.Sprite);
                        break;
                    case 2:
                        fishImage = this.returnGreenedImage(this.Sprite);
                        break;
                    default:
                        fishImage = this.returnGreyedImage(this.Sprite);
                        break;
                }

                return fishImage;
            }

            private Image returnBluedImage(Image sourceImage)
            {
                var colorMatrix = new ColorMatrix(new[]
                {
                            new[]{.30f, 0f, 0f, 0, 0},
                            new[]{0f, .30f, 0f, 0, 0},
                            new[]{0f, 0f, .1f, 0, 0},
                            new float[]{0, 0, 0, 1, 0},
                            new float[]{0, 0, 0, 0, 1}
                        });


                return applyColorMatrix(sourceImage, colorMatrix);
            }

            private Image returnGreenedImage(Image sourceImage)
            {
                var colorMatrix = new ColorMatrix(new[]
                {
                            new[]{0f, 0f, 0f, 0, 0},
                            new[]{0f, 1f, 0f, 0, 0},
                            new[]{0f, 0f, 0f, 0, 0},
                            new float[]{0, 0, 0, 1, 0},
                            new float[]{0, 0, 0, 0, 1}
                        });


                return applyColorMatrix(sourceImage, colorMatrix);
            }

            private Image returnGreyedImage(Image sourceImage)
            {
                var colorMatrix = new ColorMatrix(new[]
                {
                            new[]{.3f, .3f, .3f, 0, 0},
                            new[]{.59f, .59f, .59f, 0, 0},
                            new[]{.11f, .11f, .11f, 0, 0},
                            new float[]{0, 0, 0, 1, 0},
                            new float[]{0, 0, 0, 0, 1}
                        });


                return applyColorMatrix(sourceImage, colorMatrix);
            }

            private static Bitmap applyColorMatrix(Image sourceImage, ColorMatrix colorMatrix)
            {
                var bmp32BppSource = getArgbCopy(sourceImage);
                var bmp32BppDest = new Bitmap(bmp32BppSource.Width, bmp32BppSource.Height, PixelFormat.Format32bppArgb);


                using (var graphics = Graphics.FromImage(bmp32BppDest))
                {
                    var bmpAttributes = new ImageAttributes();
                    bmpAttributes.SetColorMatrix(colorMatrix);

                    graphics.DrawImage(bmp32BppSource, new Rectangle(0, 0, bmp32BppSource.Width, bmp32BppSource.Height),
                                        0, 0, bmp32BppSource.Width, bmp32BppSource.Height, GraphicsUnit.Pixel, bmpAttributes);


                }


                bmp32BppSource.Dispose();


                return bmp32BppDest;
            }

            private static Bitmap getArgbCopy(Image sourceImage)
            {
                var bmpNew = new Bitmap(sourceImage.Width, sourceImage.Height, PixelFormat.Format32bppArgb);

                using (var graphics = Graphics.FromImage(bmpNew))
                {
                    graphics.DrawImage(sourceImage, new Rectangle(0, 0, bmpNew.Width, bmpNew.Height),
                        new Rectangle(0, 0, bmpNew.Width, bmpNew.Height), GraphicsUnit.Pixel);
                    graphics.Flush();
                }

                return bmpNew;
            }
            #endregion
        }
    }

