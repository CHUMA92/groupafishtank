﻿using System;
using System.Drawing;
using System.Linq;
using FishTank.Model.Fishes;

namespace FishTank.View.Sprites.FishSprites
{
    /// <summary>
    ///     Responsible for drawing the tiger shark sprite on the screen.
    /// </summary>
    public class TigerSharkSprite : SharkSprite
    {
        #region Properties

        /// <summary>
        /// Gets the stripe coordinates.
        /// </summary>
        /// <value>
        /// The stripe coordinates.
        /// </value>
        private Point[] StripeCoordinates
        {
            get
            {
                var stripeCoordinates = new[]
                {
                    new Point {X = 50, Y = 7},
                    new Point {X = 60, Y = 0},
                    new Point {X = 90, Y = 40},
                    new Point {X = 70, Y = 43}
                };
                return stripeCoordinates;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TigerSharkSprite" /> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        public TigerSharkSprite(Fish fish)
            : base(fish)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Draws a fish
        /// Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        /// <exception cref="ArgumentNullException">graphics</exception>
        public override void Paint(Graphics graphics)
        {
            base.Paint(graphics);
            this.drawStripes(graphics, X, Y);
        }

        private void drawStripes(Graphics graphics, int offsetX, int offsetY)
        {
            var redBrush = new SolidBrush(Color.Red);
            var adjustedCoordinates = this.StripeCoordinates;

            ApplyScaleToImagePoints(adjustedCoordinates);

            switch (TheFish.FacingDirection)
            {
                case Fish.Direction.Right:
                    adjustedCoordinates = adjustedCoordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();
                    break;
                case Fish.Direction.Left:
                    adjustedCoordinates = adjustedCoordinates.Select(point => new Point(AdjustedWidth - point.X + offsetX, point.Y + offsetY)).ToArray();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            graphics.FillPolygon(redBrush, adjustedCoordinates);
        }

        #endregion
    }
}