﻿
using System.Drawing;
using System.Linq;

namespace FishTank.View.Sprites
{
    /// <summary>
    /// 
    /// </summary>
    public class FishFoodSprite
    {
        private readonly int foodWidth;
        private readonly int foodHeight;

        /// <summary>
        /// Gets the stripe coordinates.
        /// </summary>
        /// <value>
        /// The stripe coordinates.
        /// </value>
        private Point[] FoodCoordinates
        {
            get
            {
                var foodCoordinates = new[]
                {
                    new Point {X = 0, Y = 0},
                    new Point {X = this.foodWidth, Y = 0},
                    new Point {X = this.foodWidth, Y = this.foodHeight},
                    new Point {X = 0, Y = this.foodHeight}
                };
                return foodCoordinates;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FishFoodSprite"/> class.
        /// </summary>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        public FishFoodSprite(int width, int height)
        {
            this.foodWidth = width;
            this.foodHeight = height;
        }

        /// <summary>
        /// Paints the specified graphics.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="offsetX">The offset x.</param>
        /// <param name="offsetY">The offset y.</param>
        public void Paint(Graphics graphics, int offsetX, int offsetY)
        {
            var brownBrush = new SolidBrush(Color.BurlyWood);

            var adjustedCoordinates = this.FoodCoordinates;

            adjustedCoordinates = adjustedCoordinates.Select(point => new Point(point.X + offsetX, point.Y + offsetY)).ToArray();

            graphics.FillPolygon(brownBrush, adjustedCoordinates);
        }
    }
}
