﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using FishTank.Model.Fishes;

namespace FishTank.View.Sprites
{
    /// <summary>
    /// Responsible for drawing the fish sprite on the screen.
    /// </summary>
    public abstract class FishSprite
    {
        #region Data members

        private readonly Fish fish;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the fish.
        /// </summary>
        /// <value>
        /// The fish.
        /// </value>
        protected float SizePercentage { get; private set; }

        /// <summary>
        /// Gets the fish.
        /// </summary>
        /// <value>
        /// The fish.
        /// </value>
        protected Fish TheFish
        {
            get { return this.fish; }
        }

        /// <summary>
        /// Gets the x loctaion of the fish.
        /// </summary>
        /// <value>
        /// The x loctaion of the fish.
        /// </value>
        protected int X
        {
            get { return this.fish.X; }
        }

        /// <summary>
        /// Gets the y loctaion of the fish.
        /// </summary>
        /// <value>
        /// The y loctaion of the fish.
        /// </value>
        protected int Y
        {
            get { return this.fish.Y; }
        }

        /// <summary>
        /// Gets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width { get; protected set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>
        /// The height.
        /// </value>
        public int Height { get; protected set; }

        /// <summary>
        /// Gets the width of the adjusted.
        /// </summary>
        /// <value>
        /// The width of the adjusted.
        /// </value>
        public int AdjustedWidth { get; private set; }

        /// <summary>
        /// Gets the height of the adjusted.
        /// </summary>
        /// <value>
        /// The height of the adjusted.
        /// </value>
        public int AdjustedHeight { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Prevents a default instance of the <see cref="FishSprite" /> class from being created.
        /// </summary>
        private FishSprite()
        {
            this.fish = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FishSprite" /> class.
        /// Precondition: fish != null
        /// </summary>
        /// <param name="fish">The fish.</param>
        /// <exception cref="ArgumentNullException">fish</exception>
        protected FishSprite(Fish fish)
            : this()
        {
            if (fish == null)
            {
                throw new ArgumentNullException("fish");
            }

            this.fish = fish;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FishSprite"/> class.
        /// </summary>
        /// <param name="fish">The fish.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        protected FishSprite(Fish fish, int width, int height)
        {
            this.fish = fish;
            this.Width = width;
            this.Height = height;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Draws a fish
        /// Preconditon: graphics != null
        /// </summary>
        /// <param name="graphics">The graphics object to draw the fish one</param>
        /// <exception cref="ArgumentNullException">graphics</exception>
        public abstract void Paint(Graphics graphics);

        /// <summary>
        /// Draws the body.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        /// <param name="offsetX">The offset x.</param>
        /// <param name="offsetY">The offset y.</param>
        protected abstract void DrawBody(Graphics graphics, int offsetX, int offsetY);

        /// <summary>
        /// Determines the right facing.
        /// </summary>
        /// <param name="graphics">The graphics.</param>
        public void DrawFish(Graphics graphics)
        {
            this.DrawBody(graphics, this.X, this.Y);
        }

        /// <summary>
        /// Applies the scale to image points.
        /// </summary>
        /// <param name="adjustedCoordinates">The adjusted coordinates.</param>
        protected void ApplyScaleToImagePoints(Point[] adjustedCoordinates)
        {
            var myMatrix = new Matrix();
            myMatrix.Scale(this.SizePercentage, this.SizePercentage, MatrixOrder.Append);
            myMatrix.TransformPoints(adjustedCoordinates);
        }

        /// <summary>
        /// Sets the percentage of size that the FishSprite should be drawn at. 
        /// </summary>
        /// <param name="percent">The randomom percentage that the sprite should be drawn at.</param>
        public void SetSpriteScaledSizeMeasurements(double percent)
        {
            const double minimumSizePercentage = .4;
            var calculatedPercentage = (minimumSizePercentage + ((1 - minimumSizePercentage) * percent));

            this.SizePercentage = (float) calculatedPercentage;

            this.setScaledMeasurements(calculatedPercentage);
        }

        private void setScaledMeasurements(double calculatedPercentage)
        {
            this.AdjustedWidth = Convert.ToInt32(this.Width * calculatedPercentage);
            this.AdjustedHeight = Convert.ToInt32(this.Height * calculatedPercentage);
        }

        #endregion
    }
}
